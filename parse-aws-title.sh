#!/bin/bash

logfile="log.txt"
# echo '' >${logfile}

function parse_title {
  # link="https://00000$i.awsstudygroup.com"
  link=$1
  # wget ${link}
  # wait && sleep 2

  # wget -qO- https://000004.awsstudygroup.com | perl -l -0777 -ne 'print $1 if /<title.*?>\s*(.*?)\s*<\/title/si'
  # $(awk 'BEGIN{IGNORECASE=1;FS="<title>|</title>";RS=EOF} {print $2}' index.html)

  # title=$(awk 'BEGIN{IGNORECASE=1;FS="<title>|</title>";RS=EOF} {print $2}' index.html)
  title=$(wget -qO- ${link} | perl -l -0777 -ne 'print $1 if /<h1.*?>\s*(.*?)\s*<\/h1/si')

  # if [ -z "${title}" ]; then title="Missing ..."; fi

  if [ ! -z "${title}" ]; then
    echo "#" ${title} >>${logfile}
    echo "$link" >>${logfile}
    echo "" >>${logfile}

    # rm index.html
    # wait && sleep 1
  fi
}

for i in {1..9}; do
  parse_title "https://00000$i.awsstudygroup.com"
done

for i in {10..99}; do
  parse_title "https://0000$i.awsstudygroup.com"
done

for i in {100..999}; do
  parse_title "https://000$i.awsstudygroup.com"
done
