{
  "AWSTemplateFormatVersion": "2010-09-09",
  "Resources": {
    "KMSWorkshopInstanceRole": {
      "Type": "AWS::IAM::InstanceProfile",
      "Properties": {
        "InstanceProfileName": "KMSWorkshop-InstanceInitRole",
        "Roles": [
          "KMSWorkshop-InstanceInitRole-LV"
        ],
        "Path": "/"
      }
    }
  }
}