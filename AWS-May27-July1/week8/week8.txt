Hi mọi người @channel, DEADLINE ALL LAB: 9 AM, thứ 7, 20/8/2022. Em gửi list homework tuần cuối của Phase 2 nhé! Tuần này mình chỉ có 2 bài thôi ạ :smile:
Lab 1: Getting started with serverless (https://catalog.us-east-1.prod.workshops.aws/workshops/841ce16b-9d86-48ac-a3f6-6a1b29f95d2b/en-US)
Lab 2: Single Table Design - làm trên Excel hoặc NoSQL Workbench
_____________
Link nộp lab: https://awsfcjteam.survey.fm/aws-special-force-week-8-phase-2-lab
Link feedback: https://awsfcjteam.survey.fm/aws-special-force-feedback-session-4-phase-2
Hy vọng nhận được đủ các bài lab từ anh chị nhé! Happy weekend cả nhà :heart:

Long Vu
long.vu@renovacloud.com
RenovaCloud
